/*
 * main.c
 *
 *  Created on: 27 aout 2015
 *      Author: aleonard
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <TH2.h>
#include <TApplication.h>
#include <TCanvas.h>
#include <TStyle.h>

#define ACCRHO 300
#define ACCTHETA 180
#define MAXR 150
#define PI 3.1415

void fillAccumulator(int x, int y, int** accumulator);
int findBin(double rho);
unsigned char** readBlackWhiteBmp(const char * filename, int* width, int* height);
int** computeAccumulator(unsigned char **image, int width, int height);
void findLocalMaxima(int threshold, int **array, int width, int height);
void smoothing(int **array, int width, int height);

int main(int argc, char** argv) {
	TApplication *app = new TApplication("app", &argc, argv);
	gStyle->SetOptStat(0);

	unsigned char **image;
	int width, height;
	int i, j;

	// create array where image[0][0] is bottom left of image
	// and image[width][height] is top right of image
	image = readBlackWhiteBmp("image.bmp", &width, &height);

	// print digitized image
	for (j=0; j<height; j++) {
		for (i=0; i<width; i++) {
			printf("%d ", image[i][height-1-j]);
		}
		printf("\n");
	}


	// compute accumulator
	int **accumulator;
	accumulator = computeAccumulator(image, width, height);
	//smoothing(accumulator, ACCRHO, ACCTHETA);
	//smoothing(accumulator, ACCRHO, ACCTHETA);

	printf("Looking for maxima:\n");
	findLocalMaxima(75, accumulator, ACCRHO, ACCTHETA);
	printf("Looking for maxima done\n");

	TH2F *hAccumulator = new TH2F("hAccumulator", "hAccumulator", ACCTHETA, 0, 180, ACCRHO, -MAXR, MAXR);
	for (i=1; i<=ACCTHETA; i++) {
		for (j=1; j<=ACCRHO; j++) {
			hAccumulator->SetBinContent(i, j, 1.0*accumulator[j-1][i-1]);
		}
	}
	//hAccumulator->Smooth(1, "k3a");

	// freeing memory allocated to image
	for (i=0; i<width; i++) free(image[i]);
	free(image);

	// freeing memory allocated to aacumulator
	for (i=0; i<ACCRHO; i++) free(accumulator[i]);
	free(accumulator);


	printf("Done\n");

	TCanvas *c = new TCanvas("c", "c", 600, 600);
	c->cd();
	hAccumulator->Draw("colz");
	c->Update();

	app->Run();
	return 0;

}

unsigned char** readBlackWhiteBmp(const char * filename, int* pwidth, int* pheight) {

	//read the file
	FILE* img = fopen(filename, "rb");

	// read the 54-byte header
	unsigned char header[54];
	fread(header, sizeof(unsigned char), 54, img);

	// extract width and height from header
	int width = *(int*)&header[18];
	int height = *(int*)&header[22];
	*pwidth = width;
	*pheight = height;
	int padding=0;
	while ((width*3+padding) % 4 != 0) padding++;

	int i, j;
	unsigned char **image;
	image = (unsigned char**) malloc(width * sizeof(unsigned char*));
	for (i=0; i<width; i++) {
		image[i] = (unsigned char*) malloc(height * sizeof(unsigned char));
	}

	unsigned char *data = NULL;
	for (i=0; i<height; i++ ) {
		data = (unsigned char*) malloc(sizeof(unsigned char)*(3*width+padding));
		fread(data, sizeof(unsigned char), width*3+padding, img);

		for (j=0; j<3*width; j+=3) {
			image[j/3][i] = ((data[j+2] + data[j+1] + data[j]) == 0);
		}
	}
	free(data);

	fclose(img); //close the file
	return image;

}

int** computeAccumulator(unsigned char **image, int width, int height) {
	int i, j;
	int** accumulator;
	accumulator = (int**) malloc(ACCRHO * sizeof(int*));
	for (i=0; i<ACCRHO; i++) {
		accumulator[i] = (int*) malloc(ACCTHETA * sizeof(int));
	}
	for (i=0; i<ACCRHO; i++) {
		for (j=0; j<ACCTHETA; j++) {
			accumulator[i][j] = 0;
		}
	}

	for (i=0; i<height; i++) {
		for (j=0; j<width; j++) {
			if (image[i][j]) {
				fillAccumulator(i, j, accumulator);
			}
		}
	}
	/*
	printf("     ");


	for (j=0; j<ACCTHETA; j++) {
		printf("%2d ", j);
	}
	printf("\n");
	printf("------------------------------------------------------------------\n");
	for (i=0; i<ACCRHO; i++) {
		printf("%2d | ", i);

		for (j=0; j<ACCTHETA; j++) {
			printf("%2d ", accumulator[i][j]);
		}
		printf("\n");
	}
	printf("Test %d", accumulator[0][17]);
	*/
	return accumulator;

}

void fillAccumulator(int x, int y, int** accumulator) {
	int i;
	for (i=0; i<ACCTHETA; i++) {
		double rho = x*cos((180./ACCTHETA)*(i+0.5)*PI/180.) + y*sin((180./ACCTHETA)*(i+0.5)*PI/180.);
		int bin = findBin(rho);
		if (bin >= 0 && bin < ACCRHO) {
			accumulator[bin][i]++;
		}
	}
}

int findBin(double rho) {
	int bin;
	
	for (bin = 0; bin < ACCRHO; bin++) {
		if (rho < -MAXR + bin * 2.0 * MAXR/ACCRHO && rho < -MAXR + (bin+1) * 2.0 * MAXR/ACCRHO ) return bin;
	}

	return -1;
}

void smoothing(int **array, int width, int height) {

	// make copy of array
	int **copyArray;
	copyArray = (int**) malloc(ACCRHO * sizeof(int*));
	for (int i=0; i<ACCRHO; i++) {
		copyArray[i] = (int*) malloc(ACCTHETA * sizeof(int));
	}
	
	for (int i=0; i<width; i++) {
		for (int j=0; j<height; j++) {
			copyArray[i][j] = array[i][j];
		}
	}

	for (int i=1; i<width-1; i++) {
		for (int j=1; j<height-1; j++) {
			array[i][j] = 5*copyArray[i][j];
			array[i][j] += 3*(copyArray[i+1][j] + copyArray[i-1][j] + copyArray[i][j-1] + copyArray[i][j+1]);
			array[i][j] += copyArray[i+1][j+1] + copyArray[i-1][j+1] + copyArray[i+1][j-1] + copyArray[i-1][j-1];
		}
	}

	// freeing memory allocated to aacumulator
	for (int i=0; i<ACCRHO; i++) free(copyArray[i]);
	free(copyArray);


}
void findLocalMaxima(int threshold, int **array, int width, int height) {

	int nmax = 0;
	int xmax[20];
	int ymax[20];

	for (int i=1; i<width-1; i++) {
		for (int j=1; j<height-1; j++) {
			if (array[i][j] < threshold) continue;
			if (array[i][j] >= array[i-1][j-1] && array[i][j] >= array[i][j-1]  && array[i][j] >= array[i+1][j-1] &&
					array[i][j] >= array[i-1][j]   && array[i][j] >= array[i][j]    && array[i][j] >= array[i+1][j]   &&
					array[i][j] >= array[i-1][j+1] && array[i][j] >= array[i][j+1]  && array[i][j] >= array[i+1][j+1]) {

				xmax[nmax] = i;
				ymax[nmax] = j;
				nmax++;
			}
			if (nmax == 20) break;

		}
		if (nmax == 20) break;
	}

	/*
	for (int i=0; i<nmax-1; i++) {
		for (int j=i+1; j<nmax; j++) {
			if (xmax[i] == xmax[j] && ymax[i] == ymax[j]-1) printf("%d %d belong to the same local maxima located at x=%f y=%f\n", i, j, xmax[i], ymax[i]-0.5);
			if (xmax[i] == xmax[j] && ymax[i] == ymax[j]+1) printf("%d %d belong to the same local maxima located at x=%f y=%f\n", i, j, xmax[i], ymax[i]+0.5);
			if (xmax[i] == xmax[j]-1 && ymax[i] == ymax[j]) printf("%d %d belong to the same local maxima located at x=%f y=%f\n", i, j, xmax[i]-0.5, ymax[i]);
			if (xmax[i] == xmax[j]+1 && ymax[i] == ymax[j]) printf("%d %d belong to the same local maxima located at x=%f y=%f\n", i, j, xmax[i]+0.5, ymax[i]);
			if (xmax[i] == xmax[j]+1 && ymax[i] == ymax[j]+1) printf("%d %d belong to the same local maxima located at x=%f y=%f\n", i, j, xmax[i]+0.5, ymax[i]+0.5);
			if (xmax[i] == xmax[j]+1 && ymax[i] == ymax[j]-1) printf("%d %d belong to the same local maxima located at x=%f y=%f\n", i, j, xmax[i]+0.5, ymax[i]-0.5);
			if (xmax[i] == xmax[j]-1 && ymax[i] == ymax[j]-1) printf("%d %d belong to the same local maxima located at x=%f y=%f\n", i, j, xmax[i]-0.5, ymax[i]-0.5);
			if (xmax[i] == xmax[j]-1 && ymax[i] == ymax[j]+1) printf("%d %d belong to the same local maxima located at x=%f y=%f\n", i, j, xmax[i]-0.5, ymax[i]+0.5);
		}
	}
	*/
	for (int i=0; i<nmax; i++) {
		printf("%d: rho = %f, theta = %f\n", i, -MAXR+xmax[i]*2.*MAXR/ACCRHO, ymax[i]+180./ACCTHETA); 
	}
}	






